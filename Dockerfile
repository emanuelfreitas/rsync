FROM alpine

LABEL maintainer="Emanuel Freitas <emanuelfreitas@outlook.com>"

RUN apk add --no-cache rsync && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/cache/apk/*

ENTRYPOINT ["rsync"]